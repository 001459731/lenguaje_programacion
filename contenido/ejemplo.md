# Implementar un menu

```c#:
using System;
using CommandLine;

namespace AppEjercicios
{
    public class Options
    {
        [Option('m', "menu", Required = false, HelpText = "Mostrar el menú.")]
        public bool ShowMenu { get; set; }

        [Option('o', "opcion", Required = false, HelpText = "Elegir una opción del menú.")]
        public string Opcion { get; set; } = ""; // Inicializar con una cadena vacía

        [Option("version", HelpText = "Mostrar la versión del programa.", Required = false)]
        public bool ShowVersion { get; set; }

        [Option("author", HelpText = "Mostrar el autor del programa.", Required = false)]
        public bool ShowAuthor { get; set; }
    }

    public static class MenuManager
    {
        public static void MostrarMenu()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Menú:");
            Console.WriteLine("1. Opción 1");
            Console.WriteLine("2. Opción 2");
            // Agregar más opciones según sea necesario
        }

        public static void EjecutarOpcion(string opcion)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            switch (opcion)
            {
                case "op1":
                    Console.WriteLine("Ejecutando opción 1...");
                    break;
                case "op2":
                    Console.WriteLine("Ejecutando opción 2...");
                    break;
                default:
                    Console.WriteLine("Opción no válida.");
                    break;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
              .WithParsed<Options>(opts =>
              {
                  if (opts.ShowVersion)
                  {
                      Console.WriteLine("Versión: 1.0"); // Cambia esto por la versión real de tu programa
                      return;
                  }

                  if (opts.ShowAuthor)
                  {
                      Console.WriteLine("Autor: Franklin Condori"); // Cambia esto por tu nombre
                      return;
                  }

                  if (opts.ShowMenu)
                  {
                      MenuManager.MostrarMenu();
                      if (!string.IsNullOrEmpty(opts.Opcion))
                      {
                          MenuManager.EjecutarOpcion(opts.Opcion);
                      }
                  }
                  else
                  {
                      Console.WriteLine("Escribe algo:");
                      string input = Console.ReadLine() ?? string.Empty;
                      Console.WriteLine($"Entrada recibida: {input}");
                  }
              });
        }
    }
}
```