# Estructuras basicas

Clases: Definen objetos con atributos y métodos.

Interfaces: Especifican un conjunto de métodos que una clase debe implementar.

Estructuras: Son tipos de valor que pueden contener miembros como métodos, campos y eventos.

Métodos: Contienen una serie de declaraciones para realizar una tarea específica.

Propiedades: Permiten acceder y modificar valores de campos privados.

Campos: Son variables que pertenecen a una clase o estructura.

Eventos: Permiten que las clases notifiquen a otras clases cuando ocurre algún evento.

Constructores: Métodos especiales que se llaman automáticamente cuando se crea una instancia de una clase o estructura.

Destructor (Finalizador): Se ejecuta automáticamente cuando un objeto ya no es necesario.

Delegados: Representan referencias a métodos con una lista de parámetros y un tipo de retorno.

Enumeraciones: Definen un conjunto de constantes numéricas.

Arreglos: Colecciones ordenadas de elementos del mismo tipo.

Matrices: Arreglos multidimensionales.

Colecciones genéricas: Ofrecen una alternativa más segura y eficiente que los arreglos.

Expresiones Lambda: Permiten escribir funciones anónimas de forma concisa.

Propiedades Automáticas: Permiten definir propiedades sin tener que escribir métodos de acceso y modificación.

Eventos de Clase: Permiten que una clase publique eventos que otras clases pueden suscribir.

Iteradores: Permiten recorrer secuencialmente una colección de elementos.

```java:

// 1. Clase
public class MiClase {
    // Atributos
    private int miAtributo;

    // Método
    public void MiMetodo() {
        // Lógica del método
    }
}

// 2. Interfaz
public interface IMiInterfaz {
    // Métodos que deben ser implementados por las clases que la implementen
    void Metodo1();
    int Metodo2();
}

// 3. Estructura
public struct MiEstructura {
    // Campos
    public int campo1;
    public string campo2;
}

// 4. Método
public void MiMetodo() {
    // Lógica del método
}

// 5. Propiedad
public int MiPropiedad {
    get {
        return miAtributo;
    }
    set {
        miAtributo = value;
    }
}

// 6. Campo
private int miCampo;

// 7. Evento
public event EventHandler MiEvento;

// 8. Constructor
public MiClase() {
    // Lógica del constructor
}

// 9. Destructor (Finalizador)
~MiClase() {
    // Lógica del destructor
}

// 10. Delegado
public delegate void MiDelegado(int parametro);

// 11. Enumeración
public enum Direccion {
    Izquierda,
    Derecha,
    Arriba,
    Abajo
}

// 12. Arreglo
int[] miArreglo = new int[5];

// 13. Matriz
int[,] miMatriz = new int[3, 3];

// 14. Colección Genérica
List<int> miLista = new List<int>();

// 15. Expresión Lambda
Func<int, int> cuadrado = x => x * x;

// 16. Propiedad Automática
public int MiPropiedad { get; set; }

// 17. Evento de Clase
public static event EventHandler MiEventoDeClase;

// 18. Iterador
public IEnumerable<int> NumerosPares() {
    for (int i = 0; i < 10; i += 2) {
        yield return i;
    }
}
```